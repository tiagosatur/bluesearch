This project was bootstrapped with Create React App, Express Server.

## Pre-requisites for running this project
1. Node
2. Yarn (preferentially) or npm (be aware that the commands should be different)

## Setting up the project
- Clone this project to your computer
	- git clone [URL]
- Access the root folder in the terminal
	- yarn global add nodemon (skip this step if you already have)
	- yarn
- Access the client folder
	- yarn install

## How to run this project
- yarn dev
