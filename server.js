// BASE SETUP
// ==============================================

const express = require('express')
const app = express()
const router = express.Router()
const port = process.env.PORT || 5000

var requestProxy = require('express-request-proxy')

const serverTask = function() {
	let url = `http://localhost: ${port}`
}

// ROUTES
// ==============================================

app
	.get(
		'/api/*',
		requestProxy({
			url: `http://agenciabluefoot.vtexcommercestable.com.br/api/*`
		})
	)
	.get(
		'/buscaautocomplete/*',
		requestProxy({
			url: `http://agenciabluefoot.vtexcommercestable.com.br/buscaautocomplete/*`
		})
	)

// app.get('/api/autosuggestionlist', (req, res) => {
// 	res.send({ express: 'Hello From Express' })
// })

// router.get('/about', function(req, res) {
// 	res.send({'About page'});
// });
//
//
// app.use('/', router);

// START THE SERVER
// ==============================================

app.listen(port, () => console.log(`Magic happens on port ${port}`))
