import React, { Component } from 'react'
// import '/sass/autosuggestionlist.scss';

function AutoSuggestionList(props) {
	// getSuggestions = () => {
	//
	// 	try {
	// 		fetch(this.autocompleteUrl)
	// 			.then((resp) => resp.json()) //Transforma dados em json
	// 			.then(function(data) {
	//
	// 				let productData = data.itemsReturned; //Get the results
	//
	// 				return productData.map(function(name) {
	//
	// 					let li = this.createNode('li');
	// 					let paragraph = this.createNode('p');
	//
	//
	// 							paragraph.innerHTML = `${productData.name}`;
	//
	// 							this.append(li, paragraph);
	// 							this.append(this.ul, li);
	// 				});
	//
	// 			});
	// 	} catch(error) {
	// 			console.log(error);
	// 	};
	// }

	return (
		<div className="autosugestion-wrapper">
			<h3>Você quis dizer...</h3>
			<ul id="autosugestion__list" className="autosugestion__list">
				{props.wordlist.map((item, i) => (
					<li key={i}>
						<span>{item}</span>
					</li>
				))}
			</ul>
		</div>
	)
}

export default AutoSuggestionList
