import React, { Component } from 'react';
// import './sass/productsuggestionlist.scss';

class Productsuggestionlist extends Component {
	render() {
		return (
      <div className='product-suggestion__container'>
        <h3>Productsuggestionlist</h3>
        <ul className='product-suggestion__list'>
          <li className='list-item'>Sugestion 05</li>
          <li className='list-item'>Sugestion 06</li>
          <li className='list-item'>Sugestion 07</li>
          <li className='list-item'>Sugestion 08</li>
        </ul>
      </div>

		);
	}
}

export default Productsuggestionlist;
