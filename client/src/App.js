import React, { Component } from 'react'
import { throttle, debounce } from "throttle-debounce";
import SearchBar from './SearchBar'
import AutoSuggestionList from './AutoSuggestionList'
import './sass/autosuggestionbox.scss'
// import './sass/search.scss';
import './sass/app.scss'



class App extends Component {
	constructor(props) {
		super(props)

		this.state = {
			autosugestionlist: ['Notebook', 'Celular', 'Mouse'],
			searchTerm: '',
			prevState: ''
		}

		this.autocompleteDebounced = debounce(500, this.autocompleteSearch);
		this.autocompleteThrottled = throttle(500, this.autocompleteSearch)

		// this.userisTyping = this.userisTyping.bind(this)
	}

	// componentDidMount() {
	// 	this.callApi()
	// 		.then(res => this.setState({ autosugestionlist: res.express }))
	// 		.catch(err => console.log(err))
	// }

	// callApi = async () => {
	// 	const response = await fetch(
	// 		'/buscaautocomplete/?productNameContains=pantufa'
	// 	)
	// 	const body = await response.json()
	// 	console.log(body)
	// 	if (response.status !== 200) throw Error(body.message)
	//
	// 	return body
	// }

	// https://www.peterbe.com/plog/how-to-throttle-and-debounce-an-autocomplete-input-in-react

	onChange = e => {
		this.setState({searchTerm: e.target.value}, () => {
			const currentTerm = this.state.searchTerm;

			if(currentTerm.length < 5) {
				this.autocompleteThrottled(this.state.searchTerm)
			} else {
				this.autocompleteDebounced(this.state.searchTerm)
			}
		})
	}


	autocompleteSearch = (lastTerm) => {
		// Request only when user really stopped typing
		this.waitingFor = lastTerm
		let currentTerm = this.state.searchTerm

		if(currentTerm.length !== 0 && currentTerm === this.waitingFor) {
			this.fetchAutoSuggestion()
				.then(res => this.setState({ autosugestionlist: res.express }))
				.catch(err => console.log(err))
		}


	}

	fetchAutoSuggestion = async () => {

		let termo = this.state.searchTerm
		const url = '/buscaautocomplete/?productNameContains='
		const response = await fetch(url + termo)
		const body = await response.json()

		if (response.status !== 200) throw Error(body.message)

		this.updateAutoSugestionState(body)

	}

	updateAutoSugestionState = list => {
		let wordList = list.itemsReturned
		let newSuggestions = []

		for (var i = 0; i < wordList.length; i++) {
			newSuggestions[i] = wordList[i].name
		}

		if(wordList.length === 0) {
			this.setState(() => {
				return {
					autosugestionlist: ['Não encontramos sugestões']
				}
			})
		} else {
			this.setState(() => {
				return {
					autosugestionlist: newSuggestions
				}
			})
		}



	}

	render() {
		return (
			<div className="bf-search">
				<SearchBar
					searchTerm={this.state.searchTerm}
					onChange={this.onChange}
				/>

				<div className="autosugestion-box">
					<AutoSuggestionList wordlist={this.state.autosugestionlist} />
				</div>
			</div>
		)
	}
}

export default App
