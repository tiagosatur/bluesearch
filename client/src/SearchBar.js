import React, { Component } from 'react'
import './sass/searchbar.scss'
import './icons/icon-search.svg'

class SearchBar extends Component {
	// constructor(props) {
	// 	super(props)
	// 	this.state = {
	// 		autocomplete:
	// 			'http://agenciabluefoot.vtexcommercestable.com.br/buscaautocomplete/?productNameContains={{termo}}',
	// 		fullText:
	// 			'http://agenciabluefoot.vtexcommercestable.com.br/api/catalog_system/pub/products/search/{{termo}}?map=ft',
	// 		text: ''
	// 	}
	// }

	propTypes: {
		text: React.propTypes.string
	}

	// handleSubmit(e) {
	// 	e.preventDefault
	//
	// 	if (!this.state.text.length) {
	// 		return
	// 	}
	//
	// 	const newSearch = {
	// 		text: this.state.text,
	// 		id: Date.now()
	// 	}
	// }

	render() {
		console.log(this.props.searchTerm)
		return (
			<div className="search-wrapper">
				<form className="search-form" onSubmit={this.handleSubmit}>
					<input
						id="searchBarInput"
						className="search-bar"
						type="search"
						placeholder="O que você procura?"
						onChange={this.props.onChange}
						value={this.props.searchTerm}
					/>

					<button type="submit" className="bf-search-button">
						<svg
							className="icon"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 98 98"
						>
							<path d="M96.58,89.72,77.35,70.49A43.49,43.49,0,0,0,74.12,12.7h0a43.42,43.42,0,1,0-3.63,64.65L89.72,96.58a4.85,4.85,0,1,0,6.86-6.86Zm-77-22.46a33.73,33.73,0,1,1,47.7,0A33.77,33.77,0,0,1,19.56,67.26Z" />
						</svg>
					</button>
				</form>
			</div>
		)
	}
}

export default SearchBar
